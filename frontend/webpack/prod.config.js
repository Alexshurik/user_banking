const merge = require('webpack-merge');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const commonConfig = require('./common.config');

module.exports = merge(commonConfig, {
    output: {
        path: path.join(path.dirname(__dirname), 'build'),
        filename: 'script.[hash].js'
    },
    plugins: [
        new ExtractTextPlugin({filename: 'style.[hash].css', allChunks: true})
    ]
});