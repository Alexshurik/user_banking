const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

const apiConfig = require('../src/scripts/api/config');

module.exports = {
    entry: {
        main: path.join(path.dirname(__dirname), 'src', 'scripts', 'index.js'),
    },
    output: {
        path: path.join(path.dirname(__dirname), 'build'),
        filename: 'script.js'
    },
    resolve: {
        extensions: [ '.js', '.json', '.css', '.scss' ],
        modules: [
            'node_modules'
        ],
        alias: {
            '@': path.join(path.dirname(__dirname), 'src')
        }
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['env', 'stage-0', 'react']
                }
            },
            {
                test: /\.scss$/,
                use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
                        use: [
                            {
                                loader: 'css-loader', // translates CSS into CommonJS
                                options: {
                                    url: false  // Не пытаться добавить в бандл static-файлы
                                }
                            },
                            {
                                loader: 'sass-loader', // compiles Sass to CSS
                            },
                        ],
                        fallback: 'style-loader' // used when css not extracted
                    }
                ))
            }
        ]
    },
    plugins: [
        // Настройка API в зависимости от среды
        new webpack.DefinePlugin({
            __API__: JSON.stringify(apiConfig)
        }),
        // prints more readable module names in the browser console on HMR updates
        new ExtractTextPlugin({filename: 'style.css', allChunks: true}),

        new webpack.ProvidePlugin({
            React: 'react',
            PropTypes: 'prop-types',
        }),

        new HtmlWebpackPlugin(
            {
                filename: 'index.html',
                template: 'index.html'
            }
        ),
    ],
};