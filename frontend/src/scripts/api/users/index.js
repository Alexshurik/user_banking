import axios from 'axios'

window.axios = axios;

export default {
    list({username, tnn, url}) {
        if (url) {
            return axios.get(url)
        }

        let params = {};

        if (username) {
            params['username__istartswith'] = username
        }
        if (tnn) {
            params.tnn = tnn;
        }
        return axios.get(`users/`, {params})
    },
    transfer(data){
        return axios.post(`transfer/`, data)
    }
}
