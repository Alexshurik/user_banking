const API_PROTOCOL = 'http';
const API_HOST = '138.197.187.203';
const API_PORT = '80';

const API_BASE_PATH = '/api/v1/';

module.exports = {
    API_PROTOCOL,
    API_HOST,
    API_PORT,
    API_BASE_PATH
};
