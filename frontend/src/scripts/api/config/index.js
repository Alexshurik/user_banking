let config_module;

switch (process.env.API_CONFIG_ENV) {
    case 'docker_dev':
        config_module = require('./docker_dev');
        break;
    case 'docker_prod':
        config_module = require('./docker_prod');
        break;
    default:
        config_module = require('./local');
}

module.exports = config_module;
