import axios from 'axios'

// Переменная __API__ проставляется при сборке Webpack-ом
const {API_PROTOCOL, API_HOST, API_PORT, API_BASE_PATH} = __API__;
export const initializeApi = () => axios.defaults.baseURL = `${API_PROTOCOL}://${API_HOST}:${API_PORT}${API_BASE_PATH}`;
