export const MAXIMUM_TRANSFER_AMOUNT = Number.parseFloat('999999.99');
export const MINIMAL_TRANSFER_AMOUNT = Number.parseFloat('00.01');
export const SCROLL_PERCENTAGE = 0.8;