import React from 'react'
import { render } from 'react-dom'

import style from '@/stylesheets/style.scss';
import App from './components/App.jsx'
import {initializeApi} from "./api/init";


initializeApi();


render(
    <App className="app"/>,
    document.getElementById('react-container')
);
