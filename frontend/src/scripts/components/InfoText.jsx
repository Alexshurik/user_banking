import classNames from 'class-names';


class InfoText extends React.Component {
    constructor(props) {
        super(props);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.text !== this.props.text ||
               nextProps.isError !== this.props.isError ||
               nextProps.isSuccess !== this.props.isSuccess;
    }

    render() {
        const {text, isError, isSuccess} = this.props;

        if (text) {
            return (
                <span className={classNames({
                    "info-text": true,
                    "error": isError,
                    "success": isSuccess})}>
                    {text}
                </span>
            )
        } else {
            return null;
        }
    }
}

InfoText.propTypes = {
  text: PropTypes.string,
  isError: PropTypes.bool,
  isSuccess: PropTypes.bool,
};

export default InfoText;
