import classNames from 'class-names';

import UserSearch from "./UserSearch.jsx";
import {MAXIMUM_TRANSFER_AMOUNT, MINIMAL_TRANSFER_AMOUNT, SCROLL_PERCENTAGE} from "../constants";
import usersApi from '@/scripts/api/users';
import InfoText from "./InfoText.jsx";


export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedSender: null,
            senderUsername: {
                value: '',
                isValid: null,
                infoText: '',
            },
            senderListData: {},
            senderListExpanded: false,
            fetchingInProgress: false,

            tnn: {
                value: '',
                isValid: null,
                infoText: '',
            },
            recipients_count: 0,

            amount: {
                value: '',
                isValid: null,
                infoText: '',
            },

            isFormValid: false,

            formSubmitMessage: '',
            isSubmitSuccess: null,
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.validate = this.validate.bind(this);

        this.handlerSenderSelect = this.handlerSenderSelect.bind(this);
        this.onSenderListFocusOut = this.onSenderListFocusOut.bind(this);
        this.onSenderListScroll = this.onSenderListScroll.bind(this);

        this.handleFormSubmit = this.handleFormSubmit.bind(this);

        this.inputValidators = {
            senderUsername: (value) => {
                let isValid = true;
                let errorMessage = '';

                if (value.length === 0) {
                    isValid = false;
                    errorMessage = 'Это поле не может быть пустыми'
                } else if (this.state.senderListData.count === 0) {
                    isValid = false;
                    errorMessage = 'Пользователей c данным username не найдено'
                }
                return {
                    isValid,
                    errorMessage
                }
            },
            tnn: (value) => {
                // ToDo: можно добавить правила валидации ИНН по контрольной сумме
                let isValid = true;
                let errorMessage = '';

                if (value.length === 0) {
                    isValid = false;
                    errorMessage = 'Это поле не может быть пустыми'
                } else if (this.state.recipients_count === 0) {
                    isValid = false;
                    errorMessage = 'Пользователей с данным ИНН не найдено'
                }
                return {
                    isValid,
                    errorMessage
                }
            },
            amount: (value) => {
                let isValid = true;
                let errorMessage = '';
                let stringValue = value;
                value = Number.parseFloat(value);

                if (stringValue.length === 0) {
                    isValid = false;
                    errorMessage = 'Это поле не может быть пустыми'
                } else if (isNaN(stringValue)) {
                    isValid = false;
                    errorMessage = 'Введите число вида 01234567.89'
                } else if(stringValue.split('.')[1] && stringValue.split('.')[1].length > 2) {
                    isValid = false;
                    errorMessage = 'Не должно быть больше 2ух цифр в дробной части'
                }
                else if (value > MAXIMUM_TRANSFER_AMOUNT || value < MINIMAL_TRANSFER_AMOUNT) {
                    isValid = false;
                    errorMessage = `
                    Сумма перевода должны быть в пределах [${MINIMAL_TRANSFER_AMOUNT}; ${MAXIMUM_TRANSFER_AMOUNT}]
                    `
                } else if (value * 100 % this.state.recipients_count !== 0) {
                    isValid = false;
                    errorMessage = `
                    Данную сумму невозможно поделить поровну 
                    между ${this.state.recipients_count} пользователями
                    `
                } else if (value > Number.parseFloat(this.state.selectedSender.balance)) {
                    isValid = false;
                    errorMessage = `У пользователя ${this.state.senderUsername.value} недостаточно денег на счету`
                }
                return {
                    isValid,
                    errorMessage
                }
            }
        };

        this.inputHandlers = {
            senderUsername: (value) => {
                if (value) {
                    usersApi.list({username: value})
                    .then((response) => {
                        this.setState({
                            senderListExpanded: true,
                            senderListData: response.data
                        });
                        this.validate('senderUsername', value);
                    })
                    .catch((error) => console.log(error))
                } else {
                    this.setState({
                        senderListExpanded: false,
                        senderListData: {},
                    });
                    this.validate('senderUsername', value);
                }
            },
            tnn: (value) => {
                if (value) {
                    usersApi.list({tnn: value})
                    .then((response) => {
                        this.setState({
                            recipients_count: response.data.count
                        });
                        this.validate('tnn', value);

                        if (response.data.count > 0) {
                            let tnn = {...this.state.tnn};
                            tnn.infoText = `Найдено ${response.data.count} пользователей`;
                            this.setState({
                                tnn
                            })
                        }
                    })
                    .catch((error) => console.log(error))
                } else {
                    this.setState({
                        recipients_count: 0,
                    });
                    this.validate('tnn', value);
                }
            },
        }
    }

    handleFormSubmit(event) {
        event.preventDefault();

        const data = {
            sender: this.state.selectedSender.id,
            recipients_tnn: this.state.tnn.value,
            amount: this.state.amount.value
        };

        this.setState({
            formSubmitMessage: 'Загрузка...'
        });

        usersApi.transfer(data)
            .then((response) => {
                this.setState({
                    formSubmitMessage: 'Перевод успешно осуществлен!',
                    isSubmitSuccess: true,
                }, () => {
                    this.validate('amount', this.state.amount.value)
                });

                setTimeout(() => {
                    let selectedSender = {...this.state.selectedSender};
                    let newBalance = parseFloat(selectedSender.balance) - parseFloat(this.state.amount.value);
                    selectedSender.balance = newBalance.toFixed(2);

                    this.setState({
                        selectedSender
                    }, () => {
                        this.validate('amount', this.state.amount.value)
                    });
                }, 1000);
            })
            .catch((error) => {
                console.log(error);
                // ToDo: вывод ошибок от сервера под полями формы
                this.setState({
                    formSubmitMessage: 'Ошибка (вывел в консоль)!',
                    isSubmitSuccess: false
                })
            })
            .finally(() => {
                setTimeout(() => {
                    this.setState({
                        isSubmitSuccess: null,
                        formSubmitMessage: ''
                    })
                }, 1000)
            });
    }

    handleInputChange(e) {
        const name = e.target.name;
        const value = e.target.value;

        let inputState = {...this.state[name]};
        inputState.value = value;
        this.setState(
            {
                [name]: inputState
            },
            () => {
                const handler = this.inputHandlers[name];
                if (handler) {
                    handler(value);
                } else {
                    this.validate(name, value);
                }
            }
        );
    }

    validate(name, value) {
        let inputState = {...this.state[name]};
        inputState.isValid = this.inputValidators[name](value).isValid;
        inputState.infoText = this.inputValidators[name](value).errorMessage;
        this.setState({
            [name]: inputState
        });
    }

    handlerSenderSelect(id) {
        const selectedSender = [...this.state.senderListData.results].filter((sender) => sender.id === id)[0];

        let senderUsername = {...this.state.senderUsername};
        senderUsername.value = selectedSender.username;

        this.setState({
            senderListExpanded: false,
            selectedSender,
            senderUsername,
        });
    };

    onSenderListFocusOut(e) {
        this.validate(e.target.name, e.target.value);
    }

    onSenderListScroll(event) {
        let scrollPercentage = (event.target.scrollTop + event.target.clientHeight)
            / event.target.scrollHeight;

        if (scrollPercentage > SCROLL_PERCENTAGE &&
            this.state.senderListData['next'] &&
            !this.state.fetchingInProgress) {
            this.setState({
                fetchingInProgress: true
            });

            usersApi.list({url: this.state.senderListData['next']})
                    .then((response) => {
                        let senderListData = {...response.data};
                        senderListData.results = [...this.state.senderListData.results, ...senderListData.results];
                        this.setState({
                            senderListData,
                        });
                    })
                    .catch((error) => console.log(error))
                    .finally(() => {
                        this.setState({
                            fetchingInProgress: false
                        })
                    })
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const isFormValid = this.state.senderUsername.isValid &&
                            this.state.tnn.isValid &&
                            this.state.amount.isValid;

        if (isFormValid !== prevState.isFormValid) {
            this.setState({isFormValid})
        }
    }

    render() {
        return (
            <form className="transfer-form" autoComplete="off" onSubmit={this.handleFormSubmit}>
                <div className={classNames({
                       'form-group': true,
                       'error': this.state.senderUsername.isValid === false  // Исключаем null - первичное значение
                    })}>
                    <UserSearch users={this.state.senderListData.results}
                                expanded={this.state.senderListExpanded}
                                handleUserSelect={this.handlerSenderSelect}
                                username={this.state.senderUsername.value}
                                handleChange={this.handleInputChange}
                                onBlur={this.onSenderListFocusOut}
                                onScroll={this.onSenderListScroll}/>
                    <InfoText isError={!this.state.senderUsername.isValid} text={this.state.senderUsername.infoText}/>
                </div>
                <div className={classNames({
                       'form-group': true,
                       'error': this.state.tnn.isValid === false  // Исключаем null - первичное значение
                    })}>
                    <label htmlFor="transfer-recipients">Введите ИНН получателей</label>
                    <input id="transfer-recipients"
                           value={this.state.tnn.value}
                           onChange={this.handleInputChange}
                           onBlur={this.handleInputChange}
                           className="form-input"
                           type="text"
                           name="tnn"
                           placeholder="Введите ИНН получателей"/>
                    <InfoText isError={!this.state.tnn.isValid} text={this.state.tnn.infoText}/>
                </div>
                <div className={classNames({
                       'form-group': true,
                       'error': this.state.amount.isValid === false  // Исключаем null - первичное значение
                    })}>
                    <label htmlFor="transfer-amount">Введите сумму для перевода</label>
                    <input id="transfer-amount"
                           className="form-input"
                           type="text"
                           name="amount"
                           value={this.state.amount.value}
                           onChange={this.handleInputChange}
                           onBlur={this.handleInputChange}
                           placeholder="Сумма отправки"/>
                    <InfoText isError={!this.state.amount.isValid} text={this.state.amount.infoText}/>
                </div>
                <button type="submit" disabled={!this.state.isFormValid} className="submit-button">Перевести</button>
                <InfoText isError={this.state.isSubmitSuccess === false}
                          text={this.state.formSubmitMessage}
                          isSuccess={this.state.isSubmitSuccess}/>
            </form>
        )
    }
}