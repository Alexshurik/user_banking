const UserSearch = ({
                    users,
                    handleChange,
                    onBlur,
                    username,
                    expanded,
                    handleUserSelect,
                    onScroll,
                  }) =>
    <div className="user-search">
        <label htmlFor="transfer-sender">Выберите пользователя</label>
            <input id="transfer-sender"
                   onChange={handleChange}
                   onBlur={onBlur}
                   value={username}
                   className="form-input"
                   type="text"
                   name="senderUsername"
                   placeholder="Выберите пользователя"/>
            {
                expanded ?
               <ul className="users-dropdown-list"
                   onScroll={onScroll}>
                   {users.map(({id, username, tnn, balance}, index) =>
                   <li key={index} onClick={() => handleUserSelect(id)}>
                       <span className="user-username">{username}</span>
                       <span className="user-tnn">{tnn}</span>
                       <span className="user-balance">{balance}</span>
                   </li>)}
                </ul> :
                null
            }
    </div>

UserSearch.propTypes = {
    users: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.string,
        username: PropTypes.string,
        tnn: PropTypes.string,
        balance: PropTypes.string,
    })),
    username: PropTypes.string,
    expanded: PropTypes.bool,
    handleChange: PropTypes.func,
    onBlur: PropTypes.func,
    onScroll: PropTypes.func,
    handleUserSelect: PropTypes.func,
};

export default UserSearch;