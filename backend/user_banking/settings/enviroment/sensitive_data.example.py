# Пример файла настроек приватных данных
# Таких, как: ключи от соц. сетей, данные от почтового сервиса и т.д.

# НЕ ЗАЛИВАЕТСЯ В GIT
# ОБЯЗАТЕЛЬНЫЙ ФАЙЛ

# Production-only данные БД, SECRET_KEY и т.д.
PROD_SECRET_KEY = 'change me'
PROD_DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'user_banking',
        'USER': 'user_banking',
        'PASSWORD': 'user_banking',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}

