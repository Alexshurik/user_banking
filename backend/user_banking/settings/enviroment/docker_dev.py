DEBUG = True

USE_SENTRY = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'user_banking',
        'USER': 'user_banking',
        'PASSWORD': 'user_banking',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}

ALLOWED_HOSTS = [
    '*'
]

CORS_ORIGIN_ALLOW_ALL = True
