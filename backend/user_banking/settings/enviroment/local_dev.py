DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'user_banking',
        'USER': 'user_banking',
        'PASSWORD': 'user_banking',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

ALLOWED_HOSTS = [
    '*'
]

CORS_ORIGIN_ALLOW_ALL = True
