import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

DEBUG = False

ROOT_URLCONF = 'user_banking.urls'

WSGI_APPLICATION = 'user_banking.wsgi.application'


MEDIA_URL = '/media/'
MEDIA_ROOT = '/var/user_banking/media/'

STATIC_URL = '/static/'
STATIC_ROOT = '/var/user_banking/static/'

SECRET_KEY = 'dont use me in production))'

AUTH_USER_MODEL = 'users.User'
