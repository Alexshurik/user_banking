from django.conf.urls import url
from django.urls import include
from rest_framework import routers

from user_banking.users.views import TransferView, UserViewSet


router = routers.SimpleRouter()
router.register('users', UserViewSet, base_name='user')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^transfer/$', TransferView.as_view(), name='transfer'),
]
