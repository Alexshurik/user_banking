from django.contrib.auth import get_user_model
from django.db import transaction, DataError
from django.db.models import F
from rest_framework import viewsets, mixins, status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.views import APIView

from user_banking.common.pagination import StandardPagination
from user_banking.users.serializers import UserSerializer
from user_banking.users.filters import UserFilter
from user_banking.users.serializers import TransferSerializer


User = get_user_model()


class UserViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    serializer_class = UserSerializer
    pagination_class = StandardPagination
    queryset = User.objects.all().order_by('username')
    filter_class = UserFilter
    # ToDo: улучшить
    # Можно создать специальный endpoint для фронта для получения кол-ва пользователей
    # Сейчас фронт получает кол-во пользователей из поля "count" списокового view


class TransferView(APIView):
    def post(self, request):
        serializer = TransferSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        with transaction.atomic():
            sender = request.data['sender']
            try:
                user = User.objects.select_for_update().get(pk=sender)
            except (User.DoesNotExist, ValueError):
                raise ValidationError({
                    'sender': f'Пользователя с id {sender} не существует'
                })

            if user.balance < serializer.validated_data['amount']:
                raise ValidationError({
                    'sender': f'У пользователя с id {sender} не хватает денег на счету'
                })
            user.balance -= serializer.validated_data['amount']
            user.save(update_fields=['balance'])

            recipients = serializer.validated_data['recipients']
            recipients_count = len(serializer.validated_data['recipients'])  # Проставили поле в сериалайзере
            try:
                updated_count = recipients.update(
                    balance=F('balance') + serializer.validated_data['amount'] / recipients_count
                )
            except DataError:
                raise ValidationError(
                    'Невозможно осуществить перевод, у одного из пользователей превышен лимит на счету'
                )

            # Во время транзакции кто-то удалился/добавил с таким же ИНН,
            # чтобы не потерять деньги или не перевести лишние, рейзим ошибку и откатываем транзакцию

            # Как другой вариант, можно устанавливать блокировки на строки пользователей,
            # подтягиваемых по ИНН, чтобы их нельзя было удалить
            # И обновлять только тех, id которых подтянули по ИНН
            if updated_count != recipients_count:
                raise ValidationError(
                    'Неизвестная ошибка, попробуйте позже'
                )
        return Response(status=status.HTTP_200_OK)
