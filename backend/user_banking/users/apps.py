from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'user_banking.users'
    verbose_name = 'Пользователи'
