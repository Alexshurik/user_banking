from decimal import Decimal
from django.contrib.auth import get_user_model
from django.urls import reverse
from model_mommy import mommy
from rest_framework import status
from rest_framework.test import APITestCase


User = get_user_model()


class UsersTestCase(APITestCase):
    def setUp(self):
        self.sender = mommy.make(User, balance=Decimal('100'))

        self.test_tnn = '7707083893'
        self.user_to1 = mommy.make(User, balance=Decimal('0'), tnn=self.test_tnn)
        self.user_to2 = mommy.make(User, balance=Decimal('0'), tnn=self.test_tnn)

    def test_transfer(self):
        url = reverse('api-v1:transfer')
        data = {
            'sender': self.sender.pk,
            'recipients_tnn': self.test_tnn,
            'amount': 2.12
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Проверяем, что объекты в БД обновились (для этого подтягиваем их заново)
        self.assertEqual(
            self.sender.balance - Decimal('2.12'),
            User.objects.get(pk=self.sender.pk).balance
        )
        self.assertEqual(
            self.user_to1.balance + Decimal('1.06'),
            User.objects.get(pk=self.user_to1.pk).balance
        )
        self.assertEqual(
            self.user_to2.balance + Decimal('1.06'),
            User.objects.get(pk=self.user_to2.pk).balance
        )

    def test_transfer_to_nonexistent_user_tnn(self):
        url = reverse('api-v1:transfer')
        data = {
            'sender': self.sender.pk,
            'recipients_tnn': 90102,
            'amount': 2.12
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_transfer_more_than_user_has(self):
        url = reverse('api-v1:transfer')
        data = {
            'sender': self.sender.pk,
            'recipients_tnn': self.test_tnn,
            'amount': 200  # На счету у него 100 (setUp())
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_transfer_to_no_one(self):
        url = reverse('api-v1:transfer')
        data = {
            'sender': self.sender.pk,
            'recipients_tnn': None,
            'amount': 2.12
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_transfer_from_nonexistent_user(self):
        url = reverse('api-v1:transfer')
        data = {
            'sender': 234234,
            'recipients_tnn': self.test_tnn,
            'amount': 2.12
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_transfer_too_few(self):
        url = reverse('api-v1:transfer')
        data = {
            'sender': self.sender.pk,
            'recipients_tnn': self.test_tnn,
            'amount': 0.00
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {
            'sender': self.sender.pk,
            'recipients_tnn': self.test_tnn,
            'amount': 0.001
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_transfer_maximum_allowed_value(self):
        sender = mommy.make(User, balance=Decimal('999999.99'))
        user_to = mommy.make(User, balance=Decimal('0'), tnn=7728168971)

        url = reverse('api-v1:transfer')
        data = {
            'sender': sender.pk,
            'recipients_tnn': 7728168971,
            'amount': 999999.99
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        user_to.refresh_from_db()
        self.assertEqual(user_to.balance, Decimal('999999.99'))

    def test_transfer_too_much(self):
        url = reverse('api-v1:transfer')
        data = {
            'sender': self.sender.pk,
            'recipients_tnn': self.test_tnn,
            'amount': 1000000.00
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_transfer_negative_value(self):
        url = reverse('api-v1:transfer')
        data = {
            'sender': self.sender.pk,
            'recipients_tnn': self.test_tnn,
            'amount': -1
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_transfer_indivisible_amount(self):
        url = reverse('api-v1:transfer')
        data = {
            'sender': self.sender.pk,
            'recipients_tnn': self.test_tnn,
            'amount': 3.03  # Не делится на 2
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_transfer_receiver_account_exceeded_limit(self):
        url = reverse('api-v1:transfer')
        user_to = mommy.make(User, balance=Decimal('999999.99'), tnn=7728168971)
        data = {
            'sender': self.sender.pk,
            'recipients_tnn': 7728168971,
            'amount': 0.01
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
