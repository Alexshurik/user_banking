from django.contrib.auth import get_user_model
from django.urls import reverse
from model_mommy import mommy
from rest_framework import status
from rest_framework.test import APITestCase


User = get_user_model()


class UsersTestCase(APITestCase):
    def test_username_search(self):
        # Эти пользователи должны найтись по запросу ?username__istartswith=alex
        user1 = mommy.make(User, username='alex')
        user2 = mommy.make(User, username='alex134')
        user3 = mommy.make(User, username='aLeXandr')

        # А эти нет
        mommy.make(User)
        mommy.make(User)
        mommy.make(User)

        url = reverse('api-v1:user-list')
        response = self.client.get(url, {'username__istartswith': 'alex'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        retrieved_user_ids = [
            user['id'] for user in response.data['results']
        ]
        self.assertCountEqual(retrieved_user_ids, [user1.pk, user2.pk, user3.pk])

    def test_tnn_searh(self):
        # Эти пользователи должны найтись по запросу ?tnn=7707083893
        user1 = mommy.make(User, tnn='7707083893')
        user2 = mommy.make(User, tnn='7707083893')
        user3 = mommy.make(User, tnn='7707083893')

        # А эти нет
        mommy.make(User)
        mommy.make(User)
        mommy.make(User)

        url = reverse('api-v1:user-list')
        response = self.client.get(url, {'tnn': '7707083893'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        retrieved_user_ids = [
            user['id'] for user in response.data['results']
        ]
        self.assertCountEqual(retrieved_user_ids, [user1.pk, user2.pk, user3.pk])
