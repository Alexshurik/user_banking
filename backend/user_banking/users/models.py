from decimal import Decimal

from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    # Взял длину побольше на всякий случай,
    # т.к. в других странах ИНН бывает длиннее русского
    # В реальных приложениях необходимо валидовать ИНН
    tnn = models.CharField('ИНН', max_length=30, db_index=True)
    balance = models.DecimalField('Баланс', max_digits=8, decimal_places=2)

    # Без email (можно оставить пустым) не будет нормально работать createsuperuser
    REQUIRED_FIELDS = ['tnn', 'balance', 'email']

    MAXIMUM_BALANCE_AMOUNT = Decimal('999999.99')

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
        indexes = [
            models.Index(fields=['tnn']),
            models.Index(fields=['username'])
            # case-insensitive индекс по UPPER("username") в initial-миграции
        ]
