from decimal import Decimal

from django.contrib.auth import get_user_model
from rest_framework import serializers


User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'tnn',
            'balance',
            # При желании можно и другие поля пользователя (first_name, last_name)
            # добавить в сериализацию
        )


class TransferSerializer(serializers.Serializer):
    # Валидуем только то, что пришло число.
    # Тем самым не подтягиваем в сериалайзере пользователя
    # (на него необходимо дополнительно поставить блокировку)
    sender = serializers.IntegerField()
    recipients_tnn = serializers.CharField(max_length=30)
    amount = serializers.DecimalField(
        decimal_places=2,
        max_digits=8,
        min_value=Decimal('00.01'),
        max_value=Decimal('999999.99'),
    )

    def validate(self, data):
        # ToDo: валидацаия ИНН по контрольной сумме
        tnn = data['recipients_tnn']
        recipients = User.objects.filter(tnn=data['recipients_tnn'])
        recipients_count = len(recipients)

        if recipients_count == 0:
            raise serializers.ValidationError({
                'amount': f'Пользователей с данным ИНН {tnn} нет в базе'
            })

        amount = data['amount']
        if amount * 100 % recipients_count != 0:
            raise serializers.ValidationError({
                'amount': f'Число {amount} не делится на {recipients_count} пользователей'
            })

        # Чтобы не вытаскивать еще раз получателей, добавляем поля в validated_data
        data['recipients'] = recipients
        return data
