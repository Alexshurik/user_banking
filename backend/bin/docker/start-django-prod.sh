#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset
set -o xtrace

/home/app/manage.py collectstatic --noinput
/home/app/manage.py migrate
gunicorn user_banking.wsgi:application -c /home/app/etc/gunicorn.conf.py --log-level=info --access-logfile - --error-log -